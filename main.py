import sys

def sqr(num):
    for i in range(int(num)):
        print(i**2)
    return 0

def main():
    args = sys.argv
    if not args:
        print('need args!')
        exit(1)

    num = args[1]

    sqr(num)

if __name__ == '__main__':
    main()